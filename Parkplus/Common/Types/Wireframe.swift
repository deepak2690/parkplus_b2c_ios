//
//  Wireframe.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation
import UIKit

class Wireframe: NSObject {

    private(set) weak var navigationController: UINavigationController!

    required init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

}
