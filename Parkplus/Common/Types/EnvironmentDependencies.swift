//
//  EnvironmentDependencies.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation

struct Environment {
    enum Configuration: String, EnvironmentProtocol {
        case teamId
        case gymExportMethod
        case bundleId
        case apiUrl
        case appGroup
        case notificationBundleId
        case networkPort
    }
    // Can't take risk as these configuration are being used by fatslane script
    // swiftlint:disable identifier_name
    enum ThirdParty: String, EnvironmentProtocol {
        case GoogleApiKey
    }
    // swiftlint:enable identifier_name

}

protocol EnvironmentProtocol {

}

extension RawRepresentable where Self: EnvironmentProtocol, RawValue == String {
    private func rootClassName() -> String {
        return "Environment"
    }

    private func nameItems() -> [String] {
        let tmpNameArray = String(reflecting: self).components(separatedBy: ".")
        var nameArray = [String]()

        var isName = false
        for nameItem in tmpNameArray {
            if nameItem == self.rootClassName() {
                isName = true
                continue
            }
            if !isName {
                continue
            }

            nameArray.append(nameItem)
        }

        return nameArray
    }

    func translate() -> String {
        let nameItems = self.nameItems()
        let namevalue = EnvironmentDependencies.sharedInstance.valueFor(nameItems[0], key: nameItems[1])
        return namevalue
    }
}

class EnvironmentDependencies {
    static let sharedInstance = EnvironmentDependencies()

    private var environmentConstants: [String: [String: AnyObject]]!

    init() {
        self.load()
    }

    func load() {

        let bundle = Bundle(for: EnvironmentDependencies.self)
        guard let infoDictionary = bundle.infoDictionary, let configuration = infoDictionary["Configuration"] else {
            return
        }

        guard let path = bundle.path(forResource: "EnvironmentDependencies", ofType: "plist"),
            let environments = NSDictionary(contentsOfFile: path) else {
                return
        }

        guard let environmentConstants = environments[configuration] as? [String: [String: AnyObject]] else {
            return
        }

        self.environmentConstants = environmentConstants
    }

    func valueFor(_ module: String, key: String) -> String {
        if key == "branch_key" {
            guard let branchKey = self.environmentConstants[module]?[key] as? NSDictionary,
                let liveKey = branchKey["live"] as? String else {
                    fatalError("It should not be happened")
            }
            return liveKey
        } else {
            guard let notNilKey = self.environmentConstants?[module]?[key] as? String else {
                fatalError("It should not be happened")
            }
            return notNilKey
        }
    }
}
