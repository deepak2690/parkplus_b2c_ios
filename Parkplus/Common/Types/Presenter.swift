//
//  Presenter.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation
//import BOLDNetwork
import Alamofire

enum ReachabilityStatus {
    case connected
    case notConnected
}

class Presenter {

    private var networkManager = NetworkReachabilityManager()!
    #if APP_RUNNING
    var analyticsInteractor = InteractorFactory.sharedInstance.analyticsInteractor()
    #endif

    func startInternetReachabilityListener(completion:@escaping ((_ status: ReachabilityStatus) -> Void)) {
        self.networkManager.listener = {status in
            if  self.networkManager.isReachable == true {
                completion(.connected)
            } else {
                completion(.notConnected)
            }
        }
        self.networkManager.startListening()

        if self.networkManager.networkReachabilityStatus == .notReachable {
            completion(.notConnected)
        }

    }

    func isNetworkReachable() -> Bool {
        return self.networkManager.networkReachabilityStatus != .notReachable
    }

    func stopInternetReachabilityListener() {
        self.networkManager.stopListening()
    }

}
