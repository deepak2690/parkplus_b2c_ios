// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSFont
  internal typealias Font = NSFont
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIFont
  internal typealias Font = UIFont
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Fonts

// swiftlint:disable identifier_name line_length type_body_length
internal enum FontFamily {
  internal enum FontAwesome5Brands {
    internal static let regular = FontConvertible(name: "FontAwesome5Brands-Regular", family: "Font Awesome 5 Brands", path: "FontAwesome5Brands-Regular-400.otf")
    internal static let all: [FontConvertible] = [regular]
  }
  internal enum FontAwesome5Free {
    internal static let regular = FontConvertible(name: "FontAwesome5Free-Regular", family: "Font Awesome 5 Free", path: "FontAwesome5Free-Regular-400.otf")
    internal static let solid = FontConvertible(name: "FontAwesome5Free-Solid", family: "Font Awesome 5 Free", path: "FontAwesome5Free-Solid-900.otf")
    internal static let all: [FontConvertible] = [regular, solid]
  }
  internal enum TitilliumWeb {
    internal static let regular = FontConvertible(name: "TitilliumWeb-Regular", family: "Titillium Web", path: "TitilliumWeb-Regular.ttf")
    internal static let semiBold = FontConvertible(name: "TitilliumWeb-SemiBold", family: "Titillium Web", path: "TitilliumWeb-SemiBold.ttf")
    internal static let all: [FontConvertible] = [regular, semiBold]
  }
  internal static let allCustomFonts: [FontConvertible] = [FontAwesome5Brands.all, FontAwesome5Free.all, TitilliumWeb.all].flatMap { $0 }
  internal static func registerAllCustomFonts() {
    allCustomFonts.forEach { $0.register() }
  }
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

internal struct FontConvertible {
  internal let name: String
  internal let family: String
  internal let path: String

  internal func font(size: CGFloat) -> Font! {
    return Font(font: self, size: size)
  }

  internal func register() {
    // swiftlint:disable:next conditional_returns_on_newline
    guard let url = url else { return }
    CTFontManagerRegisterFontsForURL(url as CFURL, .process, nil)
  }

  fileprivate var url: URL? {
    let bundle = Bundle(for: BundleToken.self)
    return bundle.url(forResource: path, withExtension: nil)
  }
}

internal extension Font {
  convenience init!(font: FontConvertible, size: CGFloat) {
    #if os(iOS) || os(tvOS) || os(watchOS)
    if !UIFont.fontNames(forFamilyName: font.family).contains(font.name) {
      font.register()
    }
    #elseif os(OSX)
    if let url = font.url, CTFontManagerGetScopeForURL(url as CFURL) == .none {
      font.register()
    }
    #endif

    self.init(name: font.name, size: size)
  }
}

private final class BundleToken {}
