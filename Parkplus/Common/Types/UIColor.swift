// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#08415c"></span>
  /// Alpha: 100% <br/> (0x08415cff)
  internal static let backGroundBlueColor = ColorName(rgbaValue: 0x08415cff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#eeeeee"></span>
  /// Alpha: 100% <br/> (0xeeeeeeff)
  internal static let backGroundColor = ColorName(rgbaValue: 0xeeeeeeff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#000000"></span>
  /// Alpha: 100% <br/> (0x000000ff)
  internal static let blackColor = ColorName(rgbaValue: 0x000000ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#040000"></span>
  /// Alpha: 0% <br/> (0x04000000)
  internal static let buttonBorderColor = ColorName(rgbaValue: 0x04000000)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#9d9da5"></span>
  /// Alpha: 100% <br/> (0x9d9da5ff)
  internal static let grayBackGroundColor = ColorName(rgbaValue: 0x9d9da5ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#7bcc98"></span>
  /// Alpha: 100% <br/> (0x7bcc98ff)
  internal static let greenBackGroundColor = ColorName(rgbaValue: 0x7bcc98ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#00ff00"></span>
  /// Alpha: 100% <br/> (0x00ff00ff)
  internal static let greenColor = ColorName(rgbaValue: 0x00ff00ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#37474f"></span>
  /// Alpha: 100% <br/> (0x37474fff)
  internal static let labelTextColor = ColorName(rgbaValue: 0x37474fff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f7a599"></span>
  /// Alpha: 100% <br/> (0xf7a599ff)
  internal static let lightPinkBackgroundColor = ColorName(rgbaValue: 0xf7a599ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffa726"></span>
  /// Alpha: 100% <br/> (0xffa726ff)
  internal static let muddyYellowColor = ColorName(rgbaValue: 0xffa726ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ff0000"></span>
  /// Alpha: 100% <br/> (0xff0000ff)
  internal static let redColor = ColorName(rgbaValue: 0xff0000ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#5bb0ea"></span>
  /// Alpha: 100% <br/> (0x5bb0eaff)
  internal static let skyBlueBackgroundColor = ColorName(rgbaValue: 0x5bb0eaff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4f5e64"></span>
  /// Alpha: 100% <br/> (0x4f5e64ff)
  internal static let subtitleTextColor = ColorName(rgbaValue: 0x4f5e64ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffffff"></span>
  /// Alpha: 100% <br/> (0xffffffff)
  internal static let whiteColor = ColorName(rgbaValue: 0xffffffff)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

// swiftlint:disable operator_usage_whitespace
internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
