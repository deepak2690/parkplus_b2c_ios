//
//  CALayer.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import UIKit

let kDefaultLayerColor: UIColor = UIColor(named: .muddyYellowColor)

extension CALayer {
    enum LayerType {
        case color
        case partialImage
        case fullImage
    }

    func applyCardShadow() {
        self.shadowColor = UIColor(named: .muddyYellowColor).cgColor
        self.shadowOffset = CGSize(width: 0, height: 0)
        self.shadowRadius = 5
        self.shadowOpacity = 0.10
        self.masksToBounds = false
    }

    func applyShadow(radius: CGFloat, offset: CGSize, shadowOpacity: Float = 1) {
        self.shadowColor = kDefaultLayerColor.cgColor
        self.shadowOffset = offset
        self.shadowRadius = radius
        self.shadowOpacity = shadowOpacity
        self.masksToBounds = false
    }

    func applyCustomShadow(shadowColor: UIColor, radius: CGFloat, offset: CGSize, shadowOpacity: Float) {
        self.shadowColor = shadowColor.cgColor
        self.shadowOffset = offset
        self.shadowRadius = radius
        self.shadowOpacity = shadowOpacity
        self.masksToBounds = false
    }

    func applyTopShadow (
        color: UIColor = kDefaultLayerColor,
        radius: CGFloat = 8,
        opacity: Float = 1,
        offsetX: CGFloat = 0,
        offsetY: CGFloat = 6) {
        applyCustomShadow(
            shadowColor: color,
            radius: radius,
            offset: CGSize(
                width: offsetX,
                height: offsetY
            ),
            shadowOpacity: opacity
        )
    }

    func applyBottomShadow(
        color: UIColor = kDefaultLayerColor,
        radius: CGFloat = 8,
        opacity: Float = 1,
        offsetX: CGFloat = 0,
        offsetY: CGFloat = 1) {
        applyCustomShadow(shadowColor: color,
                          radius: radius,
                          offset: CGSize(width: offsetX,
                                         height: offsetY),
                          shadowOpacity: opacity)
    }

    func applyCustomCornerRadius(cornerRadius: CGFloat) {
      self.cornerRadius = cornerRadius
      self.masksToBounds = true
    }

    func applyBorder(width: CGFloat, color: UIColor) {
        self.borderColor = color.cgColor
        self.borderWidth = width
    }

    func getTopToBottomGradientLayer(
        colors: [CGColor] = [UIColor(named: .muddyYellowColor).cgColor,
                             UIColor(named: .redColor).cgColor] ) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        return gradientLayer
    }

    func getLeftToRightGradientLayer(
        colors: [CGColor] = [UIColor(named: .muddyYellowColor).cgColor,
                             UIColor(named: .redColor).cgColor] ) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 1)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        return gradientLayer
    }

    func setBackgroundImage(image: UIImage) -> CALayer {
        let layer = CALayer()
        layer.frame = frame
        let backgroundImageView = UIImageView(frame: frame)
        backgroundImageView.image = image
        layer.insertSublayer(backgroundImageView.layer, at: 0)
        return layer
    }

    func setBackgroundColor() -> CALayer {
        let layer = CALayer()
        layer.frame = frame
        layer.backgroundColor = UIColor(named: .backGroundColor).cgColor
        return layer
    }

    func setLayerBackgroundColor() -> CALayer {
        var layer = CALayer()
        layer = setBackgroundColor()
        return layer
    }

}
