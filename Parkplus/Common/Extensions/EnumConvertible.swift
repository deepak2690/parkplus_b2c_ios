// Note: use of enumeration var in generic is instadeath
// Note: use of static var in generic is not yet supported

protocol EnumConvertible: Hashable {
    static func members() -> [Self]
}

extension EnumConvertible {
    typealias Element = Self
    static func iterate() -> AnyIterator<Element> {
        var index = 0
        return AnyIterator {
            let currentIndex = index
            let next = withUnsafePointer(to: &index, {
                $0.withMemoryRebound(to: Element.self, capacity: 1, { UnsafePointer<Element>($0).pointee })})
            index += 1
            return next.hashValue == currentIndex ? next : nil
        }
    }

    static func members() -> [Self] {
        var enumerationMembers = [Self]()
        for member in self.iterate() {
            enumerationMembers.append(member)
        }
        return enumerationMembers
    }
}
