//
//  NSLayoutConstraint.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation
import UIKit

extension NSLayoutConstraint {
    convenience init(byConstraint constraint: NSLayoutConstraint, multiplier: CGFloat) {
        self.init(
            item: constraint.firstItem as Any,
            attribute: constraint.firstAttribute,
            relatedBy: constraint.relation,
            toItem: constraint.secondItem,
            attribute: constraint.secondAttribute,
            multiplier: multiplier,
            constant: constraint.constant
        )

        self.priority = constraint.priority
        self.shouldBeArchived = constraint.shouldBeArchived
        self.identifier = constraint.identifier
    }

    convenience init(item: AnyObject, attribute: NSLayoutConstraint.Attribute, constant: CGFloat) {
        self.init(
            item: item,
            attribute: attribute,
            relatedBy: .equal,
            toItem: nil,
            attribute: .notAnAttribute,
            multiplier: 1,
            constant: constant
        )
    }
}
