//
//  Font.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

//struct FontWrapper {
//    static let FontFamily1 = FontFamily.TitilliumWeb.self
//    static let FontFamily2 = FontFamily.FontAwesome5Free.self
//    static let FontFamily3 = FontFamily.FontAwesome5Brands.self
//}
//
//enum FontSize {
//
//    case size10
//    case size12
//    case size13
//    case size14
//    case size15
//    case size16
//    case size17
//    case size18
//    case size19
//    case size20
//    case size22
//    case size24
//    case size26
//    case size28
//    case size40
//    case size75
//    // swiftlint:disable cyclomatic_complexity
//    func translate() -> CGFloat {
//        var size: CGFloat = 0
//        switch self {
//        case .size10: size = 10
//        case .size12: size = 12
//        case .size13: size = 13
//        case .size14: size = 14
//        case .size15: size = 15
//        case .size16: size = 16
//        case .size17: size = 17
//        case .size18: size = 18
//        case .size19: size = 19
//        case .size20: size = 20
//        case .size22: size = 22
//        case .size24: size = 24
//        case .size26: size = 26
//        case .size28: size = 28
//        case .size40: size = 40
//        case .size75: size = 75
//        }
//        return size
//    }
//    // swiftlint:enable cyclomatic_complexity
//}
//
//enum FontStyle {
//
//    enum FontFamily1 {
//
//        case regular(size: FontSize)
//        case semiBold(size: FontSize)
//
//        func translate() -> Font {
//            switch self {
//            case .regular(let sizeValue): return FontWrapper.FontFamily1.regular.font(size: sizeValue.translate())
//            case .semiBold(let sizeValue): return FontWrapper.FontFamily1.semiBold.font(size: sizeValue.translate())
//            }
//        }
//    }
//
//    enum FontFamily2 {
//        case regular(size: FontSize)
//        case solid(size: FontSize)
//
//        func translate() -> Font {
//            switch self {
//            case .regular(let sizeValue): return FontWrapper.FontFamily2.regular.font(size: sizeValue.translate())
//            case .solid(let sizeValue): return FontWrapper.FontFamily2.solid.font(size: sizeValue.translate())
//            }
//        }
//    }
//
//    enum FontFamily3 {
//        case regular(size: FontSize)
//
//        func translate() -> Font {
//            switch self {
//            case .regular(let sizeValue): return FontWrapper.FontFamily3.regular.font(size: sizeValue.translate())
//            }
//        }
//    }
//}
