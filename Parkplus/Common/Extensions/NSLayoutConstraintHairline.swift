//
//  NSLayoutConstraintHairline.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import UIKit

class NSLayoutConstraintHairline: NSLayoutConstraint {
    class func width() -> CGFloat {
        return 1 / UIScreen.main.scale
    }
    class func borderWidth() -> CGFloat {
        return 1.0
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        if self.constant == 1 {
            self.constant = NSLayoutConstraintHairline.width()
        }
    }
}
