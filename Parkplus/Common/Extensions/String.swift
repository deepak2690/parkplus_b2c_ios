//
//  String.swift
//  WakeApp
//
//  Created by Artur Jaworski on 15.04.2016.
//  Copyright © 2016 Nomtek. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

let kNonAcceptableCharacters = "0123456789!@#$%^&*()+=[]\\;,/{}|\":<>?~_€£¥•"

extension String {
    func rangesOfOccurrencesOf(_ string: String) -> [Range<Index>] {
        var range = self.range(of: self)
        var ranges = [Range<Index>]()
        while true {
            let tmpRange = self.range(of: string, options: [], range: range)
            guard let foundRange = tmpRange else {
                break
            }

            ranges.append(foundRange)
            range = foundRange.upperBound..<self.endIndex
        }

        return ranges
    }

    func rangesBetweenOccurrencesOf(_ string: String) -> [Range<Index>] {
        let ranges = self.rangesOfOccurrencesOf(string)
        var newRanges = [Range<Index>]()
        for index in stride(from: 0, to: ranges.count - 1, by: 2) {
            let startRange = ranges[index]
            let endRange = ranges[index + 1]

            let range = startRange.lowerBound..<endRange.upperBound
            newRanges.append(range)
        }

        return newRanges
    }

    func stringByTrimmingWhitespacesAndNewLines() -> String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }

    func stringByTrimmingWhitespaces() -> String {
        return trimmingCharacters(in: CharacterSet.whitespaces)
    }

    func isCharacterAccpetable(character: String) -> Bool {

        if character == "" {
            return true
        }

        let characterSet = CharacterSet(charactersIn: kNonAcceptableCharacters).inverted as CharacterSet
        let filtered = character.components(separatedBy: characterSet).joined()
        return character != filtered
    }

    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: font],
            context: nil
        )

        return boundingBox.height
    }

    func capitalizeFirstLetter() -> String {
        let first = String(prefix(1).capitalized)
        let other = String(dropFirst())
        return first + other
    }

    func capitalizeFirstLetterOfWord(recentString: String?) -> String {
        var finalString = ""
        if  let notNillLoccationString = recentString?.components(separatedBy: " ") {
            for  location in notNillLoccationString {
                finalString += " " + location.capitalizeFirstLetter()
            }
        }
        return finalString
    }

    func getCharactersFromString(inputString: String?, count: Int?) -> String {
        if let notNillInputString = inputString,
            notNillInputString.count > 0,
            let notNillCharactersCount = count,
            notNillCharactersCount > 0 {
            let string1 = notNillInputString
            let index1 = string1.index(string1.startIndex, offsetBy: notNillCharactersCount)
            let substring1 = string1[string1.startIndex..<index1]
            return String(substring1)
        }
        return ""
    }

    func getLocationCoordinate() -> CLLocationCoordinate2D? {
        let coordinateArray = self.components(separatedBy: ",")
        if coordinateArray.count == 2 {
            return CLLocationCoordinate2DMake(Double(coordinateArray[0])!, Double(coordinateArray[1])!)
        }
        return  nil
    }

    static func getIPAddresses() -> String {
        var addresses = [String]()

        // Get list of all interfaces on the local machine:
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0
            else { return "" }
        guard let firstAddr = ifaddr
            else { return "" }

        // For each interface ...
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            var addr = ptr.pointee.ifa_addr.pointee

            // Check for running IPv4, IPv6 interfaces. Skip the loopback interface.
            if (flags & (IFF_UP | IFF_RUNNING | IFF_LOOPBACK)) == (IFF_UP | IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {

                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(&addr,
                                    socklen_t(addr.sa_len),
                                    &hostname,
                                    socklen_t(hostname.count),
                                    nil,
                                    socklen_t(0),
                                    NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        freeifaddrs(ifaddr)

        if addresses.count > 0 {
            return addresses.last!
        }
        return ""
    }

    static func getNotNillString(spaceAtTail: Bool, optionalString: String?) -> String {

        if let notNillString = optionalString, !notNillString.isEmpty {
            if spaceAtTail == true {
                return "\(notNillString) "
            }
            return "\(notNillString)"
        }
        return ""
    }

    static func checkIfNotEmpty(optionalString: String?) -> Bool {
        if let notNillString = optionalString, !notNillString.isEmpty {
                return true
        }
        return false
    }

    func checkIfEmailValid() -> Bool {
        // swiftlint:disable line_length
        let emailRegEx = "(?:[a-zA-Z0-9!#$%\\&‘*+/=?\\^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%\\&'*+/=?\\^_`{|}" + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-" + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5" + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        // swiftlint:enable line_length
        let predicateEmail = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        let result = predicateEmail.evaluate(with: self)
        return result
    }
    
    static func fontAwesomeString(name: String) -> String {
        
        return fetchIconFontAwesome(name: name)
        
    }
    
     //Converts String to Int
    func toInt() -> Int {
            if let num = NumberFormatter().number(from: self) {
                return num.intValue
            }
            return 0
    }
    
    //For Removing whitespaces from leading & trailing
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }

    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}


public extension NSMutableAttributedString {
     static func fontAwesomeAttributedString(name: String,
                                             suffix: String?,
                                             iconSize: CGFloat,
                                             suffixSize: CGFloat?,
                                             style: FontAwesomeStyle,
                                             fontName: String) -> NSMutableAttributedString {
        
        // Initialise some variables
        var iconString = fetchIconFontAwesome(name: name)
        var suffixFontSize = iconSize
        
        // If there is some suffix text - add it to the string
        if let suffix = suffix {
                iconString = iconString + suffix
        }
        
        // If there is a suffix font size - make a note
        if let suffixSize = suffixSize {
            suffixFontSize = suffixSize
        }
        
        guard let font = UIFont(name: fontName, size: suffixFontSize) else {
            return NSMutableAttributedString()
        }
        
        // Build the initial string - using the suffix specifics
        let iconAttributed = NSMutableAttributedString(string: iconString, attributes: [NSAttributedString.Key.font:font])
        
        // Role font awesome over the icon and size according to parameter
        iconAttributed.addAttribute(NSAttributedString.Key.font, value: UIFont.iconFontOfSize(font: fontName, fontSize: iconSize, style: style), range: NSRange(location: 0,length: 1))
        
        return iconAttributed

    }
}
