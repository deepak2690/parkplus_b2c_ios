//
//  ReusableView.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableView: class { }

extension ReusableView where Self: UIView {

    static var reuseIdentifier: String {
        return String(describing: self)
    }

}
