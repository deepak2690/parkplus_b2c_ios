//
//  KeyboardVisibilityProtocol.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation
import UIKit

protocol KeyboardObserverProtocol {

    func handleViewWillAppearByKeyboardObserver()
    func handleViewDidDisappearByKeyboardObserver()

    func keyboardWillAppear(_ keyboardSize: CGSize)
    func keyboardWillDisappear(_ keyboardSize: CGSize)

}

extension KeyboardObserverProtocol where Self: UIViewController {

    func handleViewWillAppearByKeyboardObserver() {
        self.handleViewDidDisappearByKeyboardObserver()
        NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillShowNotification,
        object: nil,
        queue: nil
        ) { [weak self] (notification) in
            if let info = notification.userInfo as NSDictionary?,
                let value = info.value(forKey: UIResponder.keyboardFrameBeginUserInfoKey) as? NSValue {
                let keyboardSize: CGSize = value.cgRectValue.size
                self?.keyboardWillAppear(keyboardSize)
            }
        }

        NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillHideNotification,
        object: nil,
        queue: nil
        ) { [weak self] (notification) in
            if let info = notification.userInfo as NSDictionary?,
                let value = info.value(forKey: UIResponder.keyboardFrameBeginUserInfoKey) as? NSValue {
                let keyboardSize: CGSize = value.cgRectValue.size
                self?.keyboardWillDisappear(keyboardSize)
            }
        }
    }

    func handleViewDidDisappearByKeyboardObserver() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

}
