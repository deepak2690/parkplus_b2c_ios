//
//  Constant.swift
//  Parkplus
//
//  Created by Deepak Bansal on 03/02/20.
//  Copyright © 2020 Parviom. All rights reserved.
//

import Foundation

enum UserDefaultConstants: String {
    case instructionSliderDisplay
    case disableIntro
    case googleUserID
    case docUplaodPermission
    case documentBrowsingOnly
    case showApplyJobTutorial
    case appGroup
    case userId
    //key which is keeping token information for application
    case appToken
    //key which is keeping token information for extension
    case userToken
    //Don't let delete value againt these constan from user defaults
    case usersLoggedInEarlierInApp
    //end
    case authCodeReceived
    case deviceToken
    case oldDeviceToken
    case updateSavedViewedNotification
    case willGetJobDetailsNotification
    case updateRecentSearchNotification
    case alertUpdated
    case guestUser
    case loggedInViaFacebook
    case minimumAppVersion
    case visitUId
    case visitorUId
    case referrerId
    case lastTokeUpdated
    case userLoggedOut
    case updateRecommendedJobsOnSignInSignOut
    case sessionStart
    case sessionEnd
    case signInSignOut
    case privacyPolicyAlertShown
    case hydraETag
    case selectedDocument
    // swiftlint:disable cyclomatic_complexity
    func string() -> String {
        switch self {
        case .instructionSliderDisplay: return "InstructionSliderDisplay"
        case .disableIntro : return "DisableIntro"
        case .googleUserID : return "GoogleUserID"
        case .docUplaodPermission: return "docUplaodPermission"
        case .documentBrowsingOnly: return "documentBrowsingOnly"
        case .showApplyJobTutorial: return "showApplyJobTutorial"
        case .appGroup: return Environment.Configuration.appGroup.translate()
        case .userId: return "userId"
        case .userToken: return "userToken"
        case .usersLoggedInEarlierInApp: return "usersLoggedInEarlierInApp"
        case .authCodeReceived: return "authCodeReceived"
        case .deviceToken: return "deviceToken"
        case .oldDeviceToken: return "oldDeviceToken"
        case .updateSavedViewedNotification : return "UpdateSavedViewedNotification"
        case .updateRecentSearchNotification : return "updateRecentSearchNotification"
        case .alertUpdated : return "alertUpdated"
        case .appToken: return "appToken"
        case .guestUser: return "guestUser"
        case .loggedInViaFacebook: return "loggedInViaFacebook"
        case .minimumAppVersion: return "minimumAppVersion"
        case .visitUId: return "visitUId"
        case .visitorUId: return "visitorUId"
        case .referrerId: return "referrerId"
        case .lastTokeUpdated: return "lastTokeUpdated"
        case .userLoggedOut: return "userLoggedOut"
        case .updateRecommendedJobsOnSignInSignOut: return "updateRecommended"
        case .sessionStart: return "sessionStarted"
        case .sessionEnd: return "sessionEnded"
        case .signInSignOut: return "signInSignOut"
        case .privacyPolicyAlertShown: return "privacyPolicyAlertShown"
        case .hydraETag: return "hydraETag"
        case .willGetJobDetailsNotification: return"willGetJobDetailsNotification"
        case .selectedDocument: return "selectedDocument"
        }
    }
    // swiftlint:enable cyclomatic_complexity
}

enum FilterConstant: Int {
    case maxSelectionAllowed

    func value() -> Int {
        switch self {
        case . maxSelectionAllowed:
            return 10
        }
    }
}

enum DirectoryConstant: String {
    case directoryName
    case externalDocument

    func string() -> String {
        switch self {
        case .directoryName:
            return "UserDocuments"
        case .externalDocument:
            return "ExternalDocuments"
        }
    }
}

enum GuestUserActionToBeCompleted {
    case saveJob
    case applyJob
    case setJobAlert
    case setTitleSignOut
    case fetchRecommendedJobs
    case getUserJobs
    case getUserAlerts
    case fetchUserDocuments
    case getAppliedJobs
    case none
}

enum AppUpdate {
    case url

    func string() -> String {
        switch self {
        case .url: return "https://itunes.apple.com/us/app/jobs-now/id1328011799?mt=8"
        }
    }
}

enum URLType: String {
    case helpCentre
    case tos
    case privacyPolicy
    case followUpTips
}
enum WebViewPresentedFrom: String {
    case login
    case search
    case none
}
