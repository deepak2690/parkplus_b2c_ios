
#!/bin/sh

RSROOT=$SRCROOT/Parkplus/Resources
ORSROOT=$SRCROOT/Parkplus/Common/Types


"$PODS_ROOT/SwiftGen/bin/swiftgen" \
xcassets -t swift4 "$RSROOT/Assets.xcassets" \
--output "$ORSROOT/Assets.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" \
strings -t structured-swift4 "$RSROOT/Localization/en.lproj/Localizable.strings" \
--output "$ORSROOT/L10n.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" \
colors -t swift4 "$RSROOT/Color.json" \
--output "$ORSROOT/UIColor.swift"

"$PODS_ROOT/SwiftGen/bin/swiftgen" \
fonts -t swift4 "$RSROOT/Fonts" \
--output "$ORSROOT/FontFamily.swift"
